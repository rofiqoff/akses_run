package aksesrun.co.id.module.login;

import android.content.Context;
import android.content.Intent;

import aksesrun.co.id.view.activity.LoginView;

/**
 * Created by rofiqoff on 11/25/17.
 */

public class LoginWireframe {

    private LoginWireframe() {

    }

    private static class SingleToHelper {
        private static final LoginWireframe INSTANCE = new LoginWireframe();
    }

    public static LoginWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context) {
        Intent intent = new Intent(context, LoginView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
