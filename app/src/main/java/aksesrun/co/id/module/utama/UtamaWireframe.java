package aksesrun.co.id.module.utama;

import android.content.Context;
import android.content.Intent;

import aksesrun.co.id.view.activity.UtamaView;

/**
 * Created by rofiqoff on 11/25/17.
 */

public class UtamaWireframe {
    private UtamaWireframe() {
    }

    private static class SingleToHelper {
        private static final UtamaWireframe INSTANCE = new UtamaWireframe();
    }

    public static UtamaWireframe getIntance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context) {
        Intent intent = new Intent(context, UtamaView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
