package aksesrun.co.id.module.running_view;

import android.content.Context;
import android.content.Intent;

import aksesrun.co.id.R;
import aksesrun.co.id.view.activity.RunningView;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class RunningWireframe {

    private RunningWireframe() {

    }

    private static class SingleToHelper {
        private static final RunningWireframe INSTANCE = new RunningWireframe();
    }

    public static RunningWireframe getInstace() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context, String jenis) {
        Intent intent = new Intent(context, RunningView.class);
        intent.putExtra(context.getString(R.string.text_jenis), jenis);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
