package aksesrun.co.id.module.history;

import android.content.Context;
import android.content.Intent;

import aksesrun.co.id.view.activity.HistoryView;

/**
 * Created by rofiqoff on 11/30/17.
 */

public class HistoryWireframe {


    private HistoryWireframe() {

    }

    private static class SingleToHelper {
        private static final HistoryWireframe INSTANCE = new HistoryWireframe();
    }

    public static HistoryWireframe getInstace() {
        return HistoryWireframe.SingleToHelper.INSTANCE;
    }

    public void toView(Context context, String fromActivity) {
        Intent intent = new Intent(context, HistoryView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("from", fromActivity);
        context.startActivity(intent);
    }
}
