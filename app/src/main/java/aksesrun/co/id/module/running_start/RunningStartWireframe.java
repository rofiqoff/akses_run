package aksesrun.co.id.module.running_start;

import android.content.Context;
import android.content.Intent;

import aksesrun.co.id.R;
import aksesrun.co.id.view.activity.RunningStartView;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class RunningStartWireframe {

    private RunningStartWireframe() {

    }

    private static class SingleToHelper {
        private static final RunningStartWireframe INSTANCE = new RunningStartWireframe();
    }

    public static RunningStartWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context, String jenis) {
        Intent intent = new Intent(context, RunningStartView.class);
        intent.putExtra(context.getString(R.string.text_jenis), jenis);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
