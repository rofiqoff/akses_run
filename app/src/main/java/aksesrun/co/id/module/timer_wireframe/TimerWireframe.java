package aksesrun.co.id.module.timer_wireframe;

import android.content.Context;
import android.content.Intent;

import aksesrun.co.id.view.activity.TimerView;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class TimerWireframe {
    private TimerWireframe() {

    }

    private static class SingleToHelper {
        private static final TimerWireframe INSTANCE = new TimerWireframe();
    }

    public static TimerWireframe getInstance(){
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context) {
        Intent intent = new Intent(context, TimerView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
}
