package aksesrun.co.id.view.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ActivityRunningViewBinding;
import aksesrun.co.id.entity.model.HistoryModel;
import aksesrun.co.id.module.running_start.RunningStartWireframe;
import aksesrun.co.id.service.api.ApiService;
import aksesrun.co.id.support.Utils.Adapter;
import aksesrun.co.id.view.view_holder.HistoryViewHolder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class RunningView extends AppCompatActivity {

    private static final String TAG = RunningView.class.getSimpleName();
    ActivityRunningViewBinding content;

    ApiService apiService;

    SharedPreferences preferences;

    private String jenis, nik;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_running_view);
        content.setView(this);

        apiService = ApiService.Factory.create();

        preferences = getSharedPreferences(getString(R.string.session_name), MODE_PRIVATE);

        jenis = getIntent().getStringExtra(getString(R.string.text_jenis));
        nik = preferences.getString(getString(R.string.session_nik), null);

        initToolbar();
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        listHistory();
        content.btnRun.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.utama_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) listHistory();
        return true;
    }

    public void run() {
        RunningStartWireframe.getInstance().toView(getApplicationContext(), jenis);
    }

    void initView() {
        content.loading.setVisibility(View.VISIBLE);
        content.list.setVisibility(View.GONE);
        content.offlineMessage.setVisibility(View.GONE);
    }

    void initToolbar() {
        String jenis = getIntent().getStringExtra(getString(R.string.text_jenis));

        setSupportActionBar(content.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (jenis.equalsIgnoreCase(getString(R.string.text_office))) {
            getSupportActionBar().setTitle(getString(R.string.acsess_run_Office));
        } else if (jenis.equalsIgnoreCase(getString(R.string.text_personal))) {
            getSupportActionBar().setTitle(getString(R.string.acsess_run_Personal));
        }

        content.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void setList(List<HistoryModel> list) {
        Adapter<HistoryModel, HistoryViewHolder> adapter =
                new Adapter<HistoryModel, HistoryViewHolder>(
                        R.layout.item_history, HistoryViewHolder.class, HistoryModel.class, list) {

                    @Override
                    protected void bindView(HistoryViewHolder holder,
                                            HistoryModel model, int position) {

                        holder.onBind(getBaseContext(), model);
                    }
                };

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        content.list.setAdapter(adapter);
        content.list.setLayoutManager(layoutManager);
        content.list.setHasFixedSize(true);

    }

    void getListOffice() {
        Call<List<HistoryModel>> history = apiService.historyOffice(nik);
        history.enqueue(new Callback<List<HistoryModel>>() {
            @Override
            public void onResponse(Call<List<HistoryModel>> call, Response<List<HistoryModel>> response) {

                content.btnRun.setEnabled(true);

                content.list.setVisibility(View.VISIBLE);
                content.offlineMessage.setVisibility(View.GONE);
                content.loading.setVisibility(View.GONE);

                List<HistoryModel> list = response.body();

                setList(list);
            }

            @Override
            public void onFailure(Call<List<HistoryModel>> call, Throwable t) {

                content.offlineMessage.setVisibility(View.VISIBLE);
                content.list.setVisibility(View.GONE);
                content.loading.setVisibility(View.GONE);

                content.btnRun.setEnabled(false);

                Log.e(TAG, "error : " + t.getMessage());

            }
        });
    }

    void getListPersonal() {
        Call<List<HistoryModel>> history = apiService.historyPersonal(nik);
        history.enqueue(new Callback<List<HistoryModel>>() {
            @Override
            public void onResponse(Call<List<HistoryModel>> call, Response<List<HistoryModel>> response) {

                content.list.setVisibility(View.VISIBLE);
                content.offlineMessage.setVisibility(View.GONE);
                content.loading.setVisibility(View.GONE);

                content.btnRun.setEnabled(true);

                List<HistoryModel> list = response.body();

                setList(list);
            }

            @Override
            public void onFailure(Call<List<HistoryModel>> call, Throwable t) {

                content.offlineMessage.setVisibility(View.VISIBLE);
                content.list.setVisibility(View.GONE);
                content.loading.setVisibility(View.GONE);

                content.btnRun.setEnabled(false);

                Log.e(TAG, "error : " + t.getMessage());

            }
        });
    }

    void listHistory() {

        initView();

        if (jenis.equalsIgnoreCase(getString(R.string.text_office))) {
            getListOffice();
        } else if (jenis.equalsIgnoreCase(getString(R.string.text_personal))) {
            getListPersonal();
        }
    }

}
