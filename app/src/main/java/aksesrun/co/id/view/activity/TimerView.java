package aksesrun.co.id.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;

import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ActivityTimerViewBinding;
import aksesrun.co.id.entity.model.RunModel;
import aksesrun.co.id.module.history.HistoryWireframe;
import aksesrun.co.id.service.api.ApiService;
import aksesrun.co.id.support.StaticClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class TimerView extends AppCompatActivity implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = TimerView.class.getSimpleName();
    ActivityTimerViewBinding content;

    private ApiService apiService;
    private SharedPreferences preferences;

    long milliSecondTime, startTime, timeBuff, updateTime = 0L;
    int hour, seconds, miinutes, milliseconds;
    String idLari, durasi, jarak, lat, lng;

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Location mCurrentLocation, lStart, lEnd;

    double lat_, long_, lat_awal, long_awal, lat_akhir, long_akhir;

    double distance = 0;

    Handler handler;

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            milliSecondTime = SystemClock.uptimeMillis() - startTime;
            updateTime = timeBuff + milliSecondTime;
            seconds = (int) (updateTime / 1000);
            miinutes = seconds / 60;
            seconds = seconds % 60;

            milliseconds = (int) (updateTime % 1000);

            @SuppressLint("DefaultLocale")
            String timer = miinutes + " : " +
                    String.format("%02d", seconds) + ".";

            @SuppressLint("DefaultLocale")
            String millisecond = String.format("%02d", milliseconds);

            content.time.setText(timer);
            content.milliseconds.setText(millisecond);

            durasi = timer;

            handler.postDelayed(this, 0);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_timer_view);
        content.button.setView(this);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        content.button.buttonFinish.setEnabled(true);
        content.button.buttonResume.setEnabled(true);

        handler = new Handler();
        apiService = ApiService.Factory.create();

        preferences = getSharedPreferences(getString(R.string.session_name), MODE_PRIVATE);

        idLari = preferences.getString(getString(R.string.session_id_lari), null);

        startTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);

        content.button.btnStop.setVisibility(View.VISIBLE);

        buildGoogleApiClient();

        initToolbar();
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!mGoogleApiClient.isConnected()) mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
//        if (broadcastReceiver != null) {
//            unregisterReceiver(broadcastReceiver);
//        }
    }

    @Override
    public void onBackPressed() {

        if (content.button.btnStop.getVisibility() == View.VISIBLE) {
            Toast.makeText(TimerView.this,
                    "Klik Tombol Stop untuk menghentikan waktu",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(TimerView.this,
                    "Klik Tombol Finish untuk menyelesaikan waktu",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

//        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(1000);
//        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//
//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//
//            LocationServices.FusedLocationApi
//                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//
//            if (mLastLocation != null) {
//                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
////                setLocation(latLng, 16);
//            }

//        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            String strLocation =
                    DateFormat.getTimeInstance().format(location.getTime()) + "\n" +
                            "Latitude=" + location.getLatitude() + "\n" +
                            "Longitude=" + location.getLongitude();

            String lat = String.valueOf(location.getLatitude());
            String lng = String.valueOf(location.getLongitude());

            sendLocation(idLari, lng, lat);

            mCurrentLocation = location;

            if (lStart == null) {
                lStart = lEnd = mCurrentLocation;
            } else {
                lEnd = mCurrentLocation;
            }

            updateDistance();

//            StaticClass.showMessage(getBaseContext(), strLocation);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult.toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d(TAG, "Permission was grated");

                    try {
                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                mGoogleApiClient, mLocationRequest, this);
                    } catch (SecurityException s) {
                        StaticClass.showMessage(getBaseContext(), s.toString());
                    }

                } else {
                    StaticClass.showMessage(getBaseContext(), "Permission Denied !");
                }
                return;
        }

    }

    public void stop() {

        onStop();

        timeBuff += milliSecondTime;
        handler.removeCallbacks(runnable);

        content.button.containerButtonResumeFinish.setVisibility(View.VISIBLE);
        content.button.btnStop.setVisibility(View.GONE);
    }

    public void resume() {
        onStart();
        startTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);

        content.button.containerButtonResumeFinish.setVisibility(View.GONE);
        content.button.btnStop.setVisibility(View.VISIBLE);
    }

    public void finish() {

        updateData();

        onStop();

        content.button.containerButtonResumeFinish.setVisibility(View.VISIBLE);
        content.button.btnStop.setVisibility(View.GONE);

        content.button.buttonFinish.setEnabled(false);
        content.button.buttonResume.setEnabled(false);
    }

    void updateDistance() {
        distance = distance + (lStart.distanceTo(lEnd)/1000.00);
        jarak = String.valueOf(distance);
    }

    void initToolbar() {
        setSupportActionBar(content.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(getString(R.string.timer));
    }

    void toHistory() {
        HistoryWireframe.getInstace().toView(getBaseContext(), "timer");
        finish();
    }

    void updateData() {

        durasi = content.time.getText().toString() + content.milliseconds.getText().toString();

        Log.d(TAG, "Jarak : " + jarak);

        final Call<RunModel> finish = apiService.finishRun(idLari, durasi, jarak);
        finish.enqueue(new Callback<RunModel>() {
            @Override
            public void onResponse(Call<RunModel> call, Response<RunModel> response) {
                Log.d(TAG, "Status Update : " + response.body().getStatus());
                if (response.isSuccessful()) {
                    toHistory();
                }
            }

            @Override
            public void onFailure(Call<RunModel> call, Throwable t) {
                Log.e(TAG, "Error : " + t.getMessage());

                stop();
                finish();

                String message = "Anda harus terhubung dengan internet untuk menlanjutkan" +
                        "\nSilahkan periksa sambungan internet anda.";

                StaticClass.showMessage(getBaseContext(), message);
            }
        });
    }

    void sendLocation(String idLari, String longitude, String lattitude) {
        Call<RunModel> sendLocation = apiService.inputLatLong(idLari, longitude, lattitude);
        sendLocation.enqueue(new Callback<RunModel>() {
            @Override
            public void onResponse(Call<RunModel> call, Response<RunModel> response) {
                Log.d(TAG, "Success!");
            }

            @Override
            public void onFailure(Call<RunModel> call, Throwable t) {
                Log.e(TAG, "Error : " + t.getMessage());
                String message = "Anda harus terhubung dengan internet untuk menlanjutkan" +
                        "\nSilahkan periksa sambungan internet anda.";
                StaticClass.showMessage(getBaseContext(), message);
            }
        });
    }

    void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
}
