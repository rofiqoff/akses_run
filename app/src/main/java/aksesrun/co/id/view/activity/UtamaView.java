package aksesrun.co.id.view.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ActivityUtamaViewBinding;
import aksesrun.co.id.entity.model.JadwalLariModel;
import aksesrun.co.id.module.running_view.RunningWireframe;
import aksesrun.co.id.service.api.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rofiqoff on 11/24/17.
 */

public class UtamaView extends AppCompatActivity {

    private static final String TAG = UtamaView.class.getSimpleName();
    ActivityUtamaViewBinding content;
    SharedPreferences preferences;

    ApiService apiService;

    private String nik, namaDivisi;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_utama_view);
        content.setView(this);

        preferences = getSharedPreferences(getString(R.string.session_name), MODE_PRIVATE);

        apiService = ApiService.Factory.create();

        nik = preferences.getString(getString(R.string.session_nik), null);
        String nama = preferences.getString(getString(R.string.session_nama_karyawan), null);
        String divisi = preferences.getString(getString(R.string.session_kode_devisi), null);

        content.btnOffice.setEnabled(false);
        content.btnPersonal.setEnabled(false);

        initToolbar();
        initView(nama, divisi);
        cekJadwal();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initLoading();
        cekJadwal();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.utama_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh:
                initLoading();
                cekJadwal();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void officeRun() {
        RunningWireframe.getInstace().toView(getApplicationContext(), getString(R.string.text_office));
    }

    public void personalRun() {
        RunningWireframe.getInstace().toView(getApplicationContext(), getString(R.string.text_personal));
    }

    void initToolbar() {
        setSupportActionBar(content.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(getString(R.string.app_name));
    }

    void initLoading() {
        content.loading.setVisibility(View.VISIBLE);
        content.textMessage.setVisibility(View.GONE);
    }

    void initView(String nama, String kodeDivisi) {
        content.textNamaKaryawan.setText(nama);

        switch (kodeDivisi) {
            case "1" :
                namaDivisi = "HRD";
                break;
        }

        content.textDevisi.setText(namaDivisi);
    }

    void cekJadwal() {
        Call<JadwalLariModel> jadwalLari = apiService.jadwalLari(nik);
        jadwalLari.enqueue(new Callback<JadwalLariModel>() {
            @Override
            public void onResponse(Call<JadwalLariModel> call, Response<JadwalLariModel> response) {
                if (response.isSuccessful()) {

                    content.btnPersonal.setEnabled(true);

                    String status = response.body().getStatus().toString();

                    Log.d(TAG, "status : " + status);

                    content.loading.setVisibility(View.GONE);
                    content.textMessage.setVisibility(View.VISIBLE);

                    if (status.equalsIgnoreCase("1")) {

                        content.textMessage.setText(
                                getString(R.string.message_jadwal_available));
                        content.btnOffice.setEnabled(true);

                    } else if (status.equalsIgnoreCase("0")) {

                        content.textMessage.setText(
                                getString(R.string.message_jadwal_not_available));
                        content.btnOffice.setEnabled(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<JadwalLariModel> call, Throwable t) {
                Log.e(TAG, "Error : " + t.getMessage());
                content.loading.setVisibility(View.GONE);
                content.textMessage.setVisibility(View.VISIBLE);

                content.btnPersonal.setEnabled(false);

                content.textMessage.setText(getString(R.string.text_message_offline_2));
            }
        });
    }

}
