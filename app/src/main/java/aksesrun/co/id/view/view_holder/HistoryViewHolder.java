package aksesrun.co.id.view.view_holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Random;

import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ItemHistoryBinding;
import aksesrun.co.id.entity.model.HistoryModel;
import aksesrun.co.id.module.history.HistoryWireframe;

/**
 * Created by rofiqoff on 11/30/17.
 */

public class HistoryViewHolder extends RecyclerView.ViewHolder {

    public ItemHistoryBinding content;

    public HistoryViewHolder(View itemView) {
        super(itemView);
        content = DataBindingUtil.bind(itemView);
    }

    public void onBind(final Context context, final HistoryModel model) {
        final String id = model.getIdlari();
        String tanggal = model.getTanggal();
        String jarak = model.getJarak() + " Km";

        Random random = new Random();

        int alpha = 255;
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);

        int color = Color.argb(alpha, red, green, blue);

        content.viewCircle.setBackgroundResource(R.drawable.circle);

        GradientDrawable drawable = (GradientDrawable) content.viewCircle.getBackground();
        drawable.setColor(color);

        content.textId.setText(id);
        content.textTanggal.setText(tanggal);
        content.textJarak.setText(jarak);

        content.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences =
                        context.getSharedPreferences(
                                context.getString(R.string.session_name), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.remove(context.getString(R.string.session_id_lari));
                editor.putString(context.getString(R.string.session_id_lari), id);
                editor.apply();

                HistoryWireframe.getInstace().toView(context, "list");
            }
        });
    }

}
