package aksesrun.co.id.view.activity;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ActivityRunningStartViewBinding;
import aksesrun.co.id.entity.model.RunModel;
import aksesrun.co.id.module.timer_wireframe.TimerWireframe;
import aksesrun.co.id.service.api.ApiService;
import aksesrun.co.id.support.StaticClass;
import aksesrun.co.id.support.Utils.PermissionUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class RunningStartView extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = RunningStartView.class.getSimpleName();
    ActivityRunningStartViewBinding content;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ApiService apiService;

    GoogleMap map;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mMarker;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private String jenis, nik;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_running_start_view);
        content.setView(this);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);

        preferences = getSharedPreferences(getString(R.string.session_name), MODE_PRIVATE);
        editor = preferences.edit();
        apiService = ApiService.Factory.create();

        nik = preferences.getString(getString(R.string.session_nik), null);

        jenis = getIntent().getStringExtra(getString(R.string.text_jenis));

        initToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        content.btnStart.setEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.setOnMyLocationButtonClickListener(this);
        map.setMyLocationEnabled(true);

        buildGoogleApiClient();

        double latitude = -5.103028;
        double longitude = 112.342611;

        LatLng latLng = new LatLng(latitude, longitude);

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 4));

//        enableMyLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[]
            permissions, @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            if (mLastLocation != null) {
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                setLocation(latLng, 16);
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        setLocation(latLng, 16);

        if (mGoogleApiClient != null)
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback", "Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));

        StaticClass.showMessage(getBaseContext(), "Google Api Not Connected");
    }

    public void start() {
        startRun();
    }

    void setLocation(LatLng latLng, long zoom) {
        try {
            MarkerOptions markerOptions = new MarkerOptions();

            if (mMarker != null) {
                mMarker.remove();
            }

            markerOptions.position(latLng).title("Your Position");
            mMarker = map.addMarker(markerOptions);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error Marker : " + e.getMessage());
        }
    }

    void initToolbar() {
        setSupportActionBar(content.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.app_name));

        content.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void enableMyLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (map != null) {
            map.setMyLocationEnabled(true);
        }
    }

    void startRun() {
        Call<RunModel> startRun = apiService.startRun(nik, jenis);
        startRun.enqueue(new Callback<RunModel>() {
            @Override
            public void onResponse(Call<RunModel> call, Response<RunModel> response) {
                if (response.isSuccessful()) {

                    content.btnStart.setEnabled(false);

                    String idLari = response.body().getStatus();

                    sessionData(idLari);

                    TimerWireframe.getInstance().toView(getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<RunModel> call, Throwable t) {
                Log.e(TAG, "Error : " + t.getMessage());

                String message = "Periksa kembali sambungan internet Anda";
                StaticClass.showMessage(getBaseContext(), message);
            }
        });
    }

    void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog.newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    void sessionData(String idLari) {
        editor.remove(getString(R.string.session_id_lari));
        editor.putString(getString(R.string.session_id_lari), idLari);

        editor.commit();

        Log.d(TAG, "id_lari : " + idLari);
    }

    void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
}
