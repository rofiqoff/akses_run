package aksesrun.co.id.view.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.List;

import aksesrun.co.id.BuildConfig;
import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ActivityLoginViewBinding;
import aksesrun.co.id.entity.model.KaryawanModel;
import aksesrun.co.id.module.utama.UtamaWireframe;
import aksesrun.co.id.service.api.ApiService;
import aksesrun.co.id.support.StaticClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginView extends AppCompatActivity {

    private static final String TAG = LoginView.class.getSimpleName();
    ActivityLoginViewBinding content;
    ApiService apiService;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_login_view);
        content.rootButon.setView(this);

        apiService = ApiService.Factory.create();

        content.rootButon.containerButton.setVisibility(View.VISIBLE);
        content.rootButon.loading.containerLoading.setVisibility(View.GONE);

        preferences = getSharedPreferences(getString(R.string.session_name), MODE_PRIVATE);
        editor = preferences.edit();

        String status_login = preferences.getString(getString(R.string.session_login), null);
        if (status_login != null) {
            if (status_login.equalsIgnoreCase("true")) {
                toMenuUtama();
            }
        }

    }

    public void reset() {
        StaticClass.clearText(content.rootInput.edtNik);
        StaticClass.clearText(content.rootInput.edtPassword);
    }

    public void login() {
        if (StaticClass.checkEmptyText(content.rootInput.edtNik)
                & StaticClass.checkEmptyText(content.rootInput.edtPassword)) {

            content.rootButon.loading.containerLoading.setVisibility(View.VISIBLE);
            content.rootButon.containerButton.setVisibility(View.GONE);

            cekLogin(content.rootInput.edtNik.getText().toString().trim(),
                    content.rootInput.edtPassword.getText().toString().trim());

        }
    }

    void cekLogin(String nik, final String password) {
        Call<List<KaryawanModel>> login = apiService.dataKaryawan(nik, password);
        login.enqueue(new Callback<List<KaryawanModel>>() {
            @Override
            public void onResponse(Call<List<KaryawanModel>> call,
                                   Response<List<KaryawanModel>> response) {

                List<KaryawanModel> list = response.body();

                String status = "true";
                String nik = list.get(0).getNik_karyawan();
                String nama = list.get(0).getNama_karyawan();
                String jenisKelamin = list.get(0).getJenis_kelamin();
                String kodeDivisi = list.get(0).getKode_divisi();
                String witel = list.get(0).getWitel();
                String email = list.get(0).getEmail_karyawan();
                String noTelp = list.get(0).getNo_tlp_karyawan();
                String alamat = list.get(0).getAlamat_karyawan();

                sessionData(
                        status,
                        nik,
                        nama,
                        jenisKelamin,
                        kodeDivisi,
                        witel,
                        email,
                        noTelp,
                        alamat);

                toMenuUtama();
            }

            @Override
            public void onFailure(Call<List<KaryawanModel>> call, Throwable t) {
                String message = "Pastikan NIK dan Password Anda benar " +
                        "atau periksa sambungan internet Anda";

                content.rootButon.loading.containerLoading.setVisibility(View.GONE);
                content.rootButon.containerButton.setVisibility(View.VISIBLE);

                StaticClass.showMessage(getBaseContext(), message);

                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "error : " + t.getMessage());
                }
            }
        });
    }

    void toMenuUtama() {
        UtamaWireframe.getIntance().toView(getBaseContext());
    }

    void sessionData(String status,
                     String nik,
                     String nama,
                     String jenisKelamin,
                     String kodeDivisi,
                     String witel,
                     String email,
                     String noTelp,
                     String alamat) {

        editor.remove(getString(R.string.session_login));
        editor.remove(getString(R.string.session_nik));
        editor.remove(getString(R.string.session_nama_karyawan));
        editor.remove(getString(R.string.session_jenis_kelamin));
        editor.remove(getString(R.string.session_kode_devisi));
        editor.remove(getString(R.string.session_witel));
        editor.remove(getString(R.string.session_email));
        editor.remove(getString(R.string.session_no_telp));
        editor.remove(getString(R.string.session_alamat));

        editor.putString(getString(R.string.session_login), status);
        editor.putString(getString(R.string.session_nik), nik);
        editor.putString(getString(R.string.session_nama_karyawan), nama);
        editor.putString(getString(R.string.session_jenis_kelamin), jenisKelamin);
        editor.putString(getString(R.string.session_kode_devisi), kodeDivisi);
        editor.putString(getString(R.string.session_witel), witel);
        editor.putString(getString(R.string.session_email), email);
        editor.putString(getString(R.string.session_no_telp), noTelp);
        editor.putString(getString(R.string.session_alamat), alamat);

        editor.commit();

    }

}
