package aksesrun.co.id.view.activity;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import aksesrun.co.id.R;
import aksesrun.co.id.databinding.ActivityHistoryViewBinding;
import aksesrun.co.id.entity.model.HistoryModel;
import aksesrun.co.id.entity.model.RunDetailModel;
import aksesrun.co.id.module.utama.UtamaWireframe;
import aksesrun.co.id.service.api.ApiService;
import aksesrun.co.id.support.StaticClass;
import aksesrun.co.id.support.Utils.PermissionUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rofiqoff on 11/27/17.
 */

public class HistoryView extends AppCompatActivity implements
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = HistoryView.class.getSimpleName();
    public static final long INTERVAL = 1000 * 60;
    public static final long FASTEST_INTERVAL = 1000 * 60;
    public static final float SMALLEST_DISPLASMENT = 0.25F;

    ActivityHistoryViewBinding content;
    ApiService apiService;
    SharedPreferences preferences;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;

    GoogleMap map;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mMarker;

    ArrayList<LatLng> points;
    Polyline line;

    String idLari, fromActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_history_view);
        content.setView(this);

        apiService = ApiService.Factory.create();
        preferences = getSharedPreferences(getString(R.string.session_name), MODE_PRIVATE);
        idLari = preferences.getString(getString(R.string.session_id_lari), null);

        points = new ArrayList<>();

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);

        content.loading.setVisibility(View.VISIBLE);

        fromActivity = getIntent().getStringExtra("from");

        if (fromActivity.equalsIgnoreCase("timer")) {
            content.back.setVisibility(View.VISIBLE);
        } else if (fromActivity.equalsIgnoreCase("list")) {
            content.back.setVisibility(View.INVISIBLE);
        }

        initToolbar();
        getData();
        getDataLocation();
    }

    @Override
    public void onBackPressed() {
        String message = "Silahkan tekan tombol 'Back To Home' untuk meninggalkan halaman ini.";

        if (fromActivity.equalsIgnoreCase("timer")) {
            StaticClass.showMessage(getBaseContext(), message);
        } else if (fromActivity.equalsIgnoreCase("list")) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.utama_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
            content.loading.setVisibility(View.VISIBLE);
            getData();
            getDataLocation();
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.setMyLocationEnabled(true);

        buildGoogleApiClient();

        double latitude = -5.103028;
        double longitude = 112.342611;

        LatLng latLng = new LatLng(latitude, longitude);

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 4));

//        enableMyLocation();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[]
            permissions, @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            if (mLastLocation != null) {
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                setLocation(latLng, 16);
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        setLocation(latLng, 16);

        if (mGoogleApiClient != null)
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback", "Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));

        StaticClass.showMessage(getBaseContext(), "Google Api Not Connected");
    }

    public void backToHome() {
        UtamaWireframe.getIntance().toView(getBaseContext());
    }

    void setLocation(LatLng latLng, long zoom) {
        try {
            if (mMarker != null) {
                mMarker.remove();
            }

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng).title("Your Position");
            mMarker = map.addMarker(markerOptions);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error Marker : " + e.getMessage());
        }
    }

    void initToolbar() {

        setSupportActionBar(content.toolbar);
        getSupportActionBar().setTitle(getString(R.string.acsess_run_History));

        if (fromActivity.equalsIgnoreCase("timer")) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        } else if (fromActivity.equalsIgnoreCase("list")) {

            content.toolbar.setContentInsetsAbsolute(0, 0);
            content.toolbar.setContentInsetsRelative(0, 0);
            content.toolbar.setContentInsetStartWithNavigation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        content.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    void enableMyLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (map != null) {
            map.setMyLocationEnabled(true);
        }
    }

    void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog.newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    void getData() {
        Call<List<HistoryModel>> history = apiService.historyEndRun(idLari);
        history.enqueue(new Callback<List<HistoryModel>>() {
            @Override
            public void onResponse(Call<List<HistoryModel>> call, Response<List<HistoryModel>> response) {
                List<HistoryModel> list = response.body();
                content.loading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String id = list.get(0).getIdlari();
                    String tanggal = list.get(0).getTanggal();
                    String durasi = list.get(0).getDurasi();
                    String jarak = list.get(0).getJarak() + " Km";

                    content.textId.setText(id);
                    content.textTanggal.setText(tanggal);
                    content.textDuration.setText(durasi);
                    content.textJarak.setText(jarak);
                }
            }

            @Override
            public void onFailure(Call<List<HistoryModel>> call, Throwable t) {
                Log.e(TAG, "Error : " + t.getMessage());

                content.loading.setVisibility(View.GONE);

                String message = "Periksa kembali sambungan internet Anda!";
                StaticClass.showMessage(getBaseContext(), message);
            }
        });
    }

    void getDataLocation() {
        content.loading.setVisibility(View.VISIBLE);
        Call<List<RunDetailModel>> location = apiService.runDetail(idLari);
        location.enqueue(new Callback<List<RunDetailModel>>() {
            @Override
            public void onResponse(Call<List<RunDetailModel>> call, Response<List<RunDetailModel>> response) {
                List<RunDetailModel> location = response.body();

                double lat = Double.parseDouble(location.get(0).getLat());
                double lon = Double.parseDouble(location.get(0).getLon());

                map.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .title("Your First Position")
                );

                content.loading.setVisibility(View.GONE);

                for (int i = 0; i < location.size(); i++) {
                    double latitude = Double.parseDouble(location.get(i).getLat());
                    double longitude = Double.parseDouble(location.get(i).getLon());
                    LatLng latLng = new LatLng(latitude, longitude);
                    points.add(latLng);

                    Log.d(TAG, "lat : " + latitude + "\nlong : " + longitude);

                    drawLine(points);
                }
            }

            @Override
            public void onFailure(Call<List<RunDetailModel>> call, Throwable t) {
                Log.e(TAG, "Error : " + t.getMessage());

                content.loading.setVisibility(View.GONE);

                String message = "Periksa kembali sambungan internet Anda!";
                StaticClass.showMessage(getBaseContext(), message);
            }
        });
    }

    void drawLine(ArrayList<LatLng> points) {

        PolylineOptions options = new PolylineOptions()
                .width(5).color(Color.BLUE).geodesic(true);

        for (int i = 0; i < points.size(); i++) {
            LatLng point = points.get(i);
            options.add(point);
        }
        line = map.addPolyline(options);
    }

    void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
}
