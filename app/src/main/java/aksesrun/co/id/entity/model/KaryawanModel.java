package aksesrun.co.id.entity.model;

/**
 * Created by rofiqoff on 11/28/17.
 */

public class KaryawanModel {

    private String nik_karyawan;
    private String pass_karyawan;
    private String nama_karyawan;
    private String jenis_kelamin;
    private String kode_divisi;
    private String witel;
    private String email_karyawan;
    private String no_tlp_karyawan;
    private String alamat_karyawan;

    public KaryawanModel() {
    }

    public KaryawanModel(String nik_karyawan,
                         String nama_karyawan,
                         String jenis_kelamin,
                         String kode_divisi,
                         String witel,
                         String email_karyawan,
                         String no_tlp_karyawan,
                         String alamat_karyawan) {

        this.nik_karyawan = nik_karyawan;
        this.nama_karyawan = nama_karyawan;
        this.jenis_kelamin = jenis_kelamin;
        this.kode_divisi = kode_divisi;
        this.witel = witel;
        this.email_karyawan = email_karyawan;
        this.no_tlp_karyawan = no_tlp_karyawan;
        this.alamat_karyawan = alamat_karyawan;
    }

    public String getNik_karyawan() {
        return nik_karyawan;
    }

    public void setNik_karyawan(String nik_karyawan) {
        this.nik_karyawan = nik_karyawan;
    }

    public String getPass_karyawan() {
        return pass_karyawan;
    }

    public void setPass_karyawan(String pass_karyawan) {
        this.pass_karyawan = pass_karyawan;
    }

    public String getNama_karyawan() {
        return nama_karyawan;
    }

    public void setNama_karyawan(String nama_karyawan) {
        this.nama_karyawan = nama_karyawan;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getKode_divisi() {
        return kode_divisi;
    }

    public void setKode_divisi(String kode_divisi) {
        this.kode_divisi = kode_divisi;
    }

    public String getWitel() {
        return witel;
    }

    public void setWitel(String witel) {
        this.witel = witel;
    }

    public String getEmail_karyawan() {
        return email_karyawan;
    }

    public void setEmail_karyawan(String email_karyawan) {
        this.email_karyawan = email_karyawan;
    }

    public String getNo_tlp_karyawan() {
        return no_tlp_karyawan;
    }

    public void setNo_tlp_karyawan(String no_tlp_karyawan) {
        this.no_tlp_karyawan = no_tlp_karyawan;
    }

    public String getAlamat_karyawan() {
        return alamat_karyawan;
    }

    public void setAlamat_karyawan(String alamat_karyawan) {
        this.alamat_karyawan = alamat_karyawan;
    }
}
