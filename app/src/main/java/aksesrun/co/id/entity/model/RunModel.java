package aksesrun.co.id.entity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 11/30/17.
 */

public class RunModel {
    @SerializedName("0")
    @Expose
    private Integer _0;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer get_0() {
        return _0;
    }

    public void set_0(Integer _0) {
        this._0 = _0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
