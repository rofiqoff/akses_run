package aksesrun.co.id.entity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 11/28/17.
 */

public class JadwalLariModel {
    @SerializedName("0")
    @Expose
    private Integer _0;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer get_0() {
        return _0;
    }

    public void set_0(Integer _0) {
        this._0 = _0;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
