package aksesrun.co.id.entity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 12/1/17.
 */

public class RunDetailModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("idlari")
    @Expose
    private String idlari;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("lat")
    @Expose
    private String lat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdlari() {
        return idlari;
    }

    public void setIdlari(String idlari) {
        this.idlari = idlari;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
