package aksesrun.co.id.entity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 11/30/17.
 */

public class HistoryModel {
    @SerializedName("idlari")
    @Expose
    private String idlari;
    @SerializedName("nik_karyawan")
    @Expose
    private String nikKaryawan;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("durasi")
    @Expose
    private String durasi;
    @SerializedName("jarak")
    @Expose
    private String jarak;
    @SerializedName("jenis")
    @Expose
    private String jenis;

    public String getIdlari() {
        return idlari;
    }

    public void setIdlari(String idlari) {
        this.idlari = idlari;
    }

    public String getNikKaryawan() {
        return nikKaryawan;
    }

    public void setNikKaryawan(String nikKaryawan) {
        this.nikKaryawan = nikKaryawan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
}
