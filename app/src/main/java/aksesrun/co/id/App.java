package aksesrun.co.id;

import android.app.Application;

import aksesrun.co.id.support.ActivityLifeCycle;

/**
 * Created by rofiqoff on 11/23/17.
 */

public class App extends Application {

    private static final String TAG = App.class.getSimpleName();
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        ActivityLifeCycle.init(this);
    }

    public static synchronized App getInstance() {
        return instance;
    }
}
