package aksesrun.co.id.service.api;

import java.util.List;

import aksesrun.co.id.BuildConfig;
import aksesrun.co.id.entity.model.HistoryModel;
import aksesrun.co.id.entity.model.JadwalLariModel;
import aksesrun.co.id.entity.model.KaryawanModel;
import aksesrun.co.id.entity.model.RunDetailModel;
import aksesrun.co.id.entity.model.RunModel;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by rofiqoff on 11/28/17.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("login")
    Call<List<KaryawanModel>> dataKaryawan(@Field("nik_karyawan") String nik,
                                           @Field("pass_karyawan") String pass);

    @FormUrlEncoded
    @POST("runhdr")
    Call<RunModel> startRun(@Field("nik_karyawan") String nik,
                            @Field("jenis") String jenis);

    @FormUrlEncoded
    @POST("updatejarakdurasi")
    Call<RunModel> finishRun(@Field("idlari") String idLari,
                             @Field("durasi") String durasi,
                             @Field("jarak") String jarak);

    @FormUrlEncoded
    @POST("rundtl")
    Call<RunModel> inputLatLong(@Field("idlari") String idLari,
                                @Field("lon") String longitude,
                                @Field("lat") String langitude);

    @GET("jadwallari")
    Call<JadwalLariModel> jadwalLari(@Query("nik_karyawan") String nikKaryawan);

    @GET("listoffice")
    Call<List<HistoryModel>> historyOffice(@Query("nik_karyawan") String nikKaryawan);

    @GET("listpersonal")
    Call<List<HistoryModel>> historyPersonal(@Query("nik_karyawan") String nikKaryawan);

    @GET("getrunhdr")
    Call<List<HistoryModel>> historyEndRun(@Query("idlari") String idLari);

    @GET("getrundtl")
    Call<List<RunDetailModel>> runDetail(@Query("idlari") String idLari);

    class Factory {
        public static ApiService create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(ApiService.class);
        }
    }

}