package aksesrun.co.id.service.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import aksesrun.co.id.entity.model.KaryawanModel;

/**
 * Created by rofiqoff on 11/28/17.
 */

public class DbUserOperHelper extends SQLiteOpenHelper {

    private static final int DABASE_VERSION = 1;
    private static final String DATABASE_NAME = "running";
    private static final String TABLE_NAME = "karyawan";

    private static final String KEY_NIK = "nik";
    private static final String KEY_NAME = "nama_karyawan";
    private static final String KEY_JENIS_KELAMIN = "jenis_kelamin";
    private static final String KEY_KODE_DIVISI = "kode_divisi";
    private static final String KEY_WITEL = "witel";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_NO_TELP = "no_telp";
    private static final String KEY_ALAMAT = "alamat";

    public DbUserOperHelper(Context context) {
        super(context, DATABASE_NAME, null, DABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
                "( " +
                KEY_NIK + " VARCHAR(50)," +
                KEY_NAME + " VARCHAR(100)," +
                KEY_JENIS_KELAMIN + " VARCHAR(10)," +
                KEY_KODE_DIVISI + " VARCHAR(10)," +
                KEY_WITEL + " VARCHAR(100)," +
                KEY_EMAIL + " VARCHAR(100)," +
                KEY_NO_TELP + " VARCHAR(20)," +
                KEY_ALAMAT + " TEXT" + ")";

        database.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    public void addData(KaryawanModel data) {

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NIK, data.getNik_karyawan());
        values.put(KEY_NAME, data.getNama_karyawan());
        values.put(KEY_JENIS_KELAMIN, data.getJenis_kelamin());
        values.put(KEY_KODE_DIVISI, data.getKode_divisi());
        values.put(KEY_WITEL, data.getWitel());
        values.put(KEY_EMAIL, data.getEmail_karyawan());
        values.put(KEY_NO_TELP, data.getNo_tlp_karyawan());
        values.put(KEY_ALAMAT, data.getAlamat_karyawan());

        database.insert(TABLE_NAME, null, values);
        database.close();

    }

    public List<KaryawanModel> getAllData() {

        List<KaryawanModel> data = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                KaryawanModel datas = new KaryawanModel();

                datas.setNik_karyawan(cursor.getString(0));
                datas.setNama_karyawan(cursor.getString(1));
                datas.setJenis_kelamin(cursor.getString(2));
                datas.setKode_divisi(cursor.getString(3));
                datas.setWitel(cursor.getString(4));
                datas.setEmail_karyawan(cursor.getString(5));
                datas.setEmail_karyawan(cursor.getString(6));
                datas.setNo_tlp_karyawan(cursor.getString(7));
                datas.setAlamat_karyawan(cursor.getString(8));

                data.add(datas);
            } while (cursor.moveToNext());
        }

        return data;
    }

    public KaryawanModel getNama(String nik) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                TABLE_NAME,
                new String[]{KEY_NIK, KEY_NAME, KEY_KODE_DIVISI}, KEY_NIK + "=?",
                new String[]{nik},
                null,
                null,
                null,
                null
        );

        if (cursor != null) cursor.moveToFirst();

        KaryawanModel data = new KaryawanModel(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8)
        );

        return data;
    }

    public int getDataCount() {
        String countQery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase database = this.getReadableDatabase();

        Cursor cursor = database.rawQuery(countQery, null);

        return cursor.getCount();
    }

    public int updateData(KaryawanModel data) {

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, data.getNama_karyawan());
        values.put(KEY_NIK, data.getNik_karyawan());
        values.put(KEY_JENIS_KELAMIN, data.getJenis_kelamin());
        values.put(KEY_KODE_DIVISI, data.getKode_divisi());
        values.put(KEY_WITEL, data.getWitel());
        values.put(KEY_EMAIL, data.getEmail_karyawan());
        values.put(KEY_NO_TELP, data.getNo_tlp_karyawan());
        values.put(KEY_ALAMAT, data.getAlamat_karyawan());

        return database.update(TABLE_NAME, values, KEY_NIK + " = ?",
                new String[]{data.getNik_karyawan()});

    }

    public void deleteData(KaryawanModel data) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_NAME, KEY_NIK + " = ? ",
                new String[]{data.getNik_karyawan()});
        database.close();
    }

}
