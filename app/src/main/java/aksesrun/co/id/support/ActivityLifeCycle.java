package aksesrun.co.id.support;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Created by rofiqoff on 11/23/17.
 */

public class ActivityLifeCycle implements Application.ActivityLifecycleCallbacks {

    private static ActivityLifeCycle instance;
    private boolean foreground = false;

    public static void init(Application app) {
        if (instance == null) {
            instance = new ActivityLifeCycle();
            app.registerActivityLifecycleCallbacks(instance);
        }
    }

    private ActivityLifeCycle() {
    }

    public boolean isForeground() {
        return foreground;
    }

    public boolean isBackground() {
        return !foreground;
    }

    public static synchronized ActivityLifeCycle getInstance() {
        return instance;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        foreground = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        foreground = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
